const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers.js");
const auth = require("../auth.js");

// Route for creating a course
router.post("/create", auth.verify, (req, res) => {

	// For S39 Activity Code
	// Data needed in creation of course

	// contains all information needed in the function
	const data = {
		course: req.body, // contains name, description, price
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		courseController.addCourse(data.course).then(resultFromController => res.send(resultFromController));
	}

	else{
		res.send(false);
	}

})

// Start of S40 Code
// Route for retrieving all courses 
router.get("/all", (req, res) => {

	// Empty argument () since you will just retrieve all courses, no further info/argument needed
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));

})

// Route for retrieving all active courses
router.get("/active", (req, res) => {

	courseController.getActiveCourses().then(resultFromController => res.send(resultFromController));

})

// Route for retrieving a specific course
router.get("/:courseId", (req, res) => {

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));

})

// Route for updating a course
router.put("/update/:courseId", auth.verify, (req, res) => {

	const data = {
		course: req.body, // contains name, description, price
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params
	}

	if(data.isAdmin){
		courseController.updateCourse(data.course, data.params).then(resultFromController => res.send(resultFromController));
	}

	else{
		res.send(false);
	}
	
})

// For S40 Activity Code
// Route for archiving a course

router.patch("/archive/:courseId", auth.verify, (req, res) =>{
	const data = {
		course: req.body, // contains name, description, price
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		params: req.params
	}

	if(data.isAdmin){
		courseController.archiveCourse(data.course, data.params).then(resultFromController => res.send(resultFromController));
	}

	else{
		res.send(false);
	}
	
})

module.exports = router;