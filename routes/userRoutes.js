// Set up dependencies
const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers.js");
const auth = require("../auth.js");

// Route for checking if the user's email already exists in the database
//.post because since we will give the user's email to the computer
router.post("/checkEmail", (req, res) => {

	userController.checkIfEmailExists(req.body).then(resultFromController => res.send(resultFromController));

});

// Route for user registration
router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));

})

// Route for user authentication
// .post because in order to log in, you need to provide dtails to postman, such as username and password
router.post("/login", (req, res) => {

	userController.authenticateUser(req.body).then(resultFromController => res.send(resultFromController));

})

// Route for retrieving user details
// auth.verify serves as the middleman in retrieving the user details 
router.get("/details", auth.verify, (req, res) => {

	/*
	const userData = {
		id: 1,
		email: hillary@mail.com,
		isAdmin: true
	};
	*/
	const userData = auth.decode(req.headers.authorization);

	/*
	{userId: 1}
	*/
	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));

});


// Route for enrolling a user
router.post("/enroll", auth.verify, (req, res) => {

	const data = {
		// userId will be received from the request header
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}

	userController.enrollUser(data).then(resultFromController => res.send(resultFromController));
})



module.exports = router;